from django.urls import path

from .api_views import api_list_presentations, api_show_presentation

#only regular API endpoints since we still have not add support for HTTP methods like GET, PUT, and DELETE to the API endpoints.
#..will be added in Conference-go
urlpatterns = [
    path(
        "conferences/<int:conference_id>/presentations/",
        api_list_presentations,
        name="api_list_presentations",
    ),
    path(
        "presentations/<int:id>/",
        api_show_presentation,
        name="api_show_presentation",
    ),
]
