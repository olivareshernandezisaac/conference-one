from django.db import models
from django.urls import reverse


class State(models.Model):
    """
    The State model represents a US state with its name
    and abbreviation.

    State is a Value Object(Immutable) and, therefore, does not have a
    direct URL to view it.
    """

    name = models.CharField(max_length=20)
    abbreviation = models.CharField(max_length=2)

    def __str__(self):
        return f"{self.abbreviation}"

    class Meta:
        ordering = ("abbreviation",)  # Default ordering for State


class Location(models.Model):
    """
    The Location model describes the place at which an
    Event takes place, like a hotel or conference center.
    """

    name = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    room_count = models.PositiveSmallIntegerField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    state = models.ForeignKey(
        State,
        related_name="+",  # do not create a related name on State
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_show_location", kwargs={"id": self.id})

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)  # Default ordering for Location


class Conference(models.Model):
    """
    The Conference model describes a specific conference.
    """

    # Has a one-to-many relationship with presentations.Presentation
    # Has a one-to-many relationship with attendees.Attendee

    name = models.CharField(max_length=200)
    starts = models.DateTimeField()
    ends = models.DateTimeField()
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    max_presentations = models.PositiveSmallIntegerField()
    max_attendees = models.PositiveIntegerField()

    location = models.ForeignKey(
        Location,
        related_name="conferences",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_show_conference", kwargs={"id": self.id})

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("starts", "name")  # Default ordering for Conference


#many (conferences) to one (location) relationship
# models.ForeignKey(Location, ...): This is the type of the field.
# It indicates that each Conference instance will have a foreign
# key pointing to a Location instance.


# related_name="conferences": This parameter specifies the
# name to use for the reverse relation from the Location
# model back to the Conference model. This means that for each Location instance,
# you can access its related conferences using the conferences attribute.

#NO USE OF RELATED NAME! Ex. events>api_views
#to access location name from conference. MANY(CONFERENCES) TO ONE(LOCATION)
# conference = Conference.objects.get(id=id)
#location = conference.location.name

#uses related name!
#to access conference queryset from locaton. ONE(LOCATION) TO MANY(CONFERENCES)
#we use the  related name to access the attributes of conference object
#conferences = Location.conferences.all()



#STATE
# A Value Object, in the context of Domain-Driven Design (DDD), is an object that represents
# a descriptive aspect of the domain with no conceptual identity. Value Objects are distinguished
# by their attributes rather than by an identifier, and they are immutable. They have no behavior
# other than accessors (methods that retrieve values of attributes).
