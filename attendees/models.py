from django.db import models
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist #3


class Attendee(models.Model):
    """
    The Attendee model represents someone that wants to attend
    a conference
    """

    email = models.EmailField()
    name = models.CharField(max_length=200)
    company_name = models.CharField(max_length=200, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    conference = models.ForeignKey(
        "events.Conference",   #typed this way to access the conference class found under events folder
        related_name="attendees",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    def create_badge(self):  #3 making sure taht we use the aggregate root(Attendee) to be able to access Badge(Value Ojbect)
        try:
            badge = self.badge         # Attempt to retrieve an existing badge for the attendee. Assuming 'badge' is the related_name for the OneToOneField in the Attendee model.
                                    #self.badge == Attendee.badge... the badge allows it to access all the attributes in the Class Badge by using the related name badge
        except ObjectDoesNotExist: # If no badge exists, create a new one for the attendee
            Badge.objects.create(attendee=self)

    def get_api_url(self): # kwargs parameter is used to pass keyword arguments to the URL pattern. In this case, you are passing a ...
        return reverse("api_show_attendee", kwargs={"id": self.id}) #..keyword argument "id" with the value self.id. self.id represents the primary key of the current instance of the Blog model.


class Badge(models.Model):
    """
    The Badge model represents the badge an attendee gets to
    wear at the conference.

    Badge is a Value Object and, therefore, does not have a
    direct URL to view it.

    One-to-One Relationship: The Badge model has a one-to-one relationship
      with the Attendee model. This means that each Badge is associated with
      exactly one Attendee, and each Attendee can have at most one Badge.
      This implies that the Badge is closely tied to the Attendee and doesn't
      have an independent identity outside of it.

    """

    created = models.DateTimeField(auto_now_add=True)

    attendee = models.OneToOneField(
        Attendee,
        related_name="badge",  #related name used to access all attributes in Badge class from Attendee class
        on_delete=models.CASCADE,
        primary_key=True,
    )
